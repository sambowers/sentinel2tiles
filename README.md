# What is this?

It's a .csv file containing the projection (EPSG) and extent (in m) of every Sentinel-2 tile (over land).

# Why did you make it?

The Sentinel-2 tiling grid is distributed as a .kml file (https://sentinel.esa.int/web/sentinel/missions/sentinel-2/data-products). I found it tricky to work with, and I haven't figured out to to make it's lat/lon values precisely match the Sentinel-2 tiles.

# How did you make it?

I extracted the names of all Sentinel-2 tiles from the ESA .kml file, used sentinelsat (https://github.com/sentinelsat/sentinelsat) to search for an example image from each tile, and downloaded its metadata tile (MTD_TL.xml). With this its straightforward to extract its projection (EPSG) and extent.

# Are there errors?

Presently, yes! They're a nuisance to correct, but I'm sorting it. In the mean time, contributions are welcome.

# Isn't there a better way?

Probably. But if, like me, you've struggled with this, I hope this is useful.

I make no guarantees that this is correct.

# Can I contribute?

By all means. In particular, if anyone can figure out whether there's a predictable pattern here that can be worked out directly from the tiles, that would remove this faff...
